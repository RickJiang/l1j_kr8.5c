package l1j.server.server.model.item;

public class L1ItemId {
	public static final int 마력을잃은아놀드장갑 = 30145;
	
	public static final int POTION_OF_HEALING = 40010;

	public static final int B_POTION_OF_HEALING = 140010;

	public static final int C_POTION_OF_HEALING = 240010;

	public static final int POTION_OF_EXTRA_HEALING = 40011;

	public static final int B_POTION_OF_EXTRA_HEALING = 140011;

	public static final int POTION_OF_GREATER_HEALING = 40012;

	public static final int B_POTION_OF_GREATER_HEALING = 140012;

	public static final int POTION_OF_HASTE_SELF = 40013;

	public static final int B_POTION_OF_HASTE_SELF = 140013;

	public static final int POTION_OF_GREATER_HASTE_SELF = 40018;

	public static final int B_POTION_OF_GREATER_HASTE_SELF = 140018;

	public static final int POTION_OF_EMOTION_BRAVERY = 40014;

	public static final int B_POTION_OF_EMOTION_BRAVERY = 140014;

	public static final int POTION_OF_MANA = 40015;

	public static final int B_POTION_OF_MANA = 140015;

	public static final int POTION_OF_EMOTION_WISDOM = 40016;

	public static final int B_POTION_OF_EMOTION_WISDOM = 140016;

	public static final int POTION_OF_CURE_POISON = 40017;

	public static final int CONDENSED_POTION_OF_HEALING = 40019;

	public static final int CONDENSED_POTION_OF_EXTRA_HEALING = 40020;

	public static final int CONDENSED_POTION_OF_GREATER_HEALING = 40021;

	public static final int POTION_OF_BLINDNESS = 40025;

	public static final int SCROLL_OF_ENCHANT_ARMOR = 40074;

	public static final int B_SCROLL_OF_ENCHANT_ARMOR = 140074;

	public static final int C_SCROLL_OF_ENCHANT_ARMOR = 240074;

	public static final int SCROLL_OF_ENCHANT_WEAPON = 40087;

	public static final int B_SCROLL_OF_ENCHANT_WEAPON = 140087;

	public static final int C_SCROLL_OF_ENCHANT_WEAPON = 240087;

	public static final int SCROLL_OF_ENCHANT_QUEST_WEAPON = 40660;

	public static final int ADENA = 40308;

	public static final int SCROLL_OF_ENCHANT_FANTASY_ARMOR = 40127;

	public static final int SCROLL_OF_ENCHANT_FANTASY_WEAPON = 40128;

	public static final int PANS_LEATHER = 40519;

	public static final int SCROLL_OF_DOLL_CHANGE = 100001;
	public static final int HIGH_CHARACTER_TRADE = 100002;
	public static final int LOW_CHARACTER_TRADE = 100003;
	public static final int DRAGON_RUBY = 1000002;
	public static final int DRAGON_SAPPHIRE = 1000003;
	public static final int DRAGON_DIAMOND = 1000004;
	public static final int DRAGON_DIAMOND1 = 410139; //드래곤 다이아몬드 교환불가
	public static final int BEGINNER_SCROLL = 1000010;
	public static final int GIRAN_SCROLL = 1000011;
	public static final int GIRANCAVE_BOXKEY = 430026;
	public static final int DRAGONDUNGEON_SCROLL = 500216;
	public static final int OREN_SCROLL = 410062;
	public static final int RaDungeon_SCROLL = 410135;

	public static final int SCROLL_OF_DEMON_CONTRACT	= 1000012;

	public static final int SCROLL_OF_CHAOS_SPIRIT		= 1000021;
	public static final int SCROLL_OF_CORRUPT_SPIRIT	= 1000022;
	public static final int SCROLL_OF_BALLACAS_SPIRIT 	= 1000023;
	public static final int SCROLL_OF_ANTARAS_SPIRIT	= 1000024;
	public static final int SCROLL_OF_LINDBIOR_SPIRIT	= 1000025;
	public static final int SCROLL_OF_PAPURION_SPIRIT	= 1000026;
	public static final int SCROLL_OF_DEATHKNIGHT_SPIRIT= 1000027;
	public static final int SCROLL_OF_BAPPOMAT_SPIRIT	= 1000028;
	public static final int SCROLL_OF_BALLOG_SPIRIT		= 1000029;
	public static final int SCROLL_OF_ARES_SPIRIT		= 1000030;

	public static final int ANCIENT_SCROLL_OF_CHAOS_SPIRIT		= 1000031;
	public static final int ANCIENT_SCROLL_OF_CORRUPT_SPIRIT	= 1000032;
	public static final int ANCIENT_SCROLL_OF_BALLACAS_SPIRIT 	= 1000033;
	public static final int ANCIENT_SCROLL_OF_ANTARAS_SPIRIT	= 1000034;
	public static final int ANCIENT_SCROLL_OF_LINDBIOR_SPIRIT	= 1000035;
	public static final int ANCIENT_SCROLL_OF_PAPURION_SPIRIT	= 1000036;
	public static final int ANCIENT_SCROLL_OF_DEATHKNIGHT_SPIRIT= 1000037;
	public static final int ANCIENT_SCROLL_OF_BAPPOMAT_SPIRIT	= 1000038;
	public static final int ANCIENT_SCROLL_OF_BALLOG_SPIRIT		= 1000039;
	public static final int ANCIENT_SCROLL_OF_ARES_SPIRIT		= 1000040;

	public static final int DRUGA_POKET							= 410039; //드루가의 주머니
	public static final int DRAGON_KEY							= 410040; //드래곤 키
	public static final int DRAGON_PEARL 						= 410063; //드래곤 진주
	public static final int DRAGON_PEARL1 						= 410137; //드래곤 진주 교환불가
	public static final int EMERALD                             = 410064; //드래곤 에메랄드	
	public static final int EMERALD1                            = 410138; //드래곤 에메랄드 교환불가
	public static final int Inadril_T_ScrollA                   = 410066; //인나드릴 일반
	public static final int Inadril_T_ScrollB                   = 410067; //인나드릴 축
	public static final int Inadril_T_ScrollC                   = 410068; //인나드릴 저주
	public static final int Pure_white_Scroll                   = 410083; //스냅퍼강화주문서
	public static final int Roomtis_Scroll                      = 410089; //룸티스 강화 주문서
	public static final int ROOMTIS_RED_EARRING                 = 22229; // 룸티스의 붉은빛 귀걸이
	public static final int ROOMTIS_BLUE_EARRING                = 22230; // 룸티스의 푸른빛 귀걸이
	public static final int ROOMTIS_PURPLE_EARRING              = 22231; // 룸티스의 보랏빛 귀걸이
	public static final int MAGIC_BREATH                        = 410094; //마력의 숨결

	public static final int PSY_CHAMPION                        = 447012;
	public static final int PSY_BIRD                            = 447013;
	public static final int PSY_GANGNAM_STYLE                   = 447014;
	
	// 펫배송

	public static final int KILLTON_CONTRACT                    = 87050;
	public static final int MERIN_CONTRACT                      = 87051;
	public static final int KILLTON_PIPE                        = 87052;
	public static final int MERIN_PIPE                          = 87053;
	
	// 숨겨진 계곡 리뉴얼 아이템.
	public static final int IVORYTOWER_ARMOR_SCROLL = 30027; // 상아탑의 갑옷 마법 주문서
	public static final int IVORYTOWER_WEAPON_SCROLL = 30028; // 상아탑의 무기 마법 주문서
	
	public static final int MONSTER_TOENAIL = 30029; // 몬스터의 발톱
	public static final int MONSTER_TOOTH = 30030; // 몬스터의 이빨
	public static final int RUST_HELM = 30031; // 녹슨 투구
	public static final int RUST_GLOVE = 30032; // 녹슨 장갑
	public static final int RUST_BOOTS = 30033; // 녹슨 장화
	
	public static final int INSTRUCTOR_PRESENT_POKET_1 = 30034; 	// 교관의 선물 주머니
	public static final int INSTRUCTOR_PRESENT_POKET_2 = 30035; 	// 교관의 선물 주머니
	public static final int INSTRUCTOR_PRESENT_POKET_3 = 30036; 	// 교관의 선물 주머니
	public static final int INSTRUCTOR_PRESENT_POKET_4 = 30037; 	// 교관의 선물 주머니
	public static final int INSTRUCTOR_PRESENT_POKET_5 = 30038; 	// 교관의 선물 주머니
	public static final int INSTRUCTOR_PRESENT_POKET_6 = 30039; 	// 교관의 선물 주머니
	
	public static final int EXP_POTION3 = 5000059;
	public static final int EXP_POTION4 = 5000060;
	
	public static final int BASE_TRAINING_TOKEN = 30040; // 기초 훈련 증표
	public static final int PUNITIVE_EXPEDITION_TOKEN  = 30041; // 토벌의 증표
	public static final int VARIETY_DRAGON_BONE = 30042; // 변종 드래곤의 뼈
	
	public static final int PUNITIVE_EXPEDITION_POKET = 30043; // 토벌 대원의 주머니
	public static final int PUNITIVE_EXPEDITION_BEAD = 30044; // 빛나는 구슬
	
	public static final int DRAGON_BONE_POKET = 30045; // 드래곤뼈 수집꾼의 주머니
	public static final int DRAGON_BONE_BEAD = 30046; // 영롱한 구슬
	
	public static final int IVORYTOWER_SACRED_HELM = 22300; 	// 상아탑 견고한 방어구
	public static final int IVORYTOWER_SACRED_ARMOR = 22301;
	public static final int IVORYTOWER_SACRED_GLOVE = 22303;
	public static final int IVORYTOWER_SACRED_BOOTS = 22304;
	public static final int IVORYTOWER_SACRED_SHIELD = 22305;
	public static final int IVORYTOWER_SACRED_CLOAK = 22302;
	
	public static final int IVORYTOWER_STRONG_HELM = 22306; // 상아탑 신성한 방어구
	public static final int IVORYTOWER_STRONG_ARMOR = 22307;
	public static final int IVORYTOWER_STRONG_GLOVE = 22309;
	public static final int IVORYTOWER_STRONG_BOOTS = 22310;
	public static final int IVORYTOWER_STRONG_SHIELD = 22311;
	public static final int IVORYTOWER_STRONG_T = 22312;
	public static final int IVORYTOWER_STRONG_CLOAK = 22308;
	
	/** 160120 업데이트 문장 **/
//	public static final int Sentence_ARMOR_SCROLL = 3000100; // 문장강화석
	public static final int Sentence1 = 900020;//성장의 문장
	public static final int Sentence2 = 900021;//회복의 문장
}
